<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LinkanSendSurvei;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Handle linkan broadcast
Route::prefix('linkan')->group(function () {
    Route::post('linksurvei', 'LinkanSendSurvei@broadcastSurvei');
});

Route::any('{path}', function () {
    return response()->json(
        [
            'code'      => 404,
            'message'   => 'Bad Request. Route not found',
            'error'     => 'Bad Request. Route not found',
            'data'      => null,
        ],
        404
    );
})->where('path', '.*');