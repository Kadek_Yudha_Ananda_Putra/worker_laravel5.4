<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefBroadcastTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_broadcast', function (Blueprint $table) {
            $table->increments('id_broadcast');
            $table->string('id_ref_jenis_survey', 255)->nullable();
            $table->string('jenis_broadcast', 255)->nullable();
            $table->integer('delay_kirim')->nullable();
            $table->integer('kirim_permenit')->nullable();
            $table->integer('jumlah_kirim')->nullable();
            $table->text('pesan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_broadcast');
    }
}
