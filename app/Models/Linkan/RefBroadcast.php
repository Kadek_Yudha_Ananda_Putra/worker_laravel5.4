<?php

namespace App\Models\Linkan;

use Illuminate\Database\Eloquent\Model;

class RefBroadcast extends Model
{
    protected $table = SCHEMA_LINKAN . '.ref_broadcast';
    protected $fillable = [
        'nama_broadcast',
        'jenis_broadcast',
        'delay_kirim',
        'kirim_permenit',
        'jumlah_kirim',
        'sudah_kirim',
    ];
}
