<?php

namespace App\Models\Linkan;

use Illuminate\Database\Eloquent\Model;

class JadwalSurvei extends Model
{

    protected $table = SCHEMA_LINKAN . '.ref_jadwal_survey';
}
