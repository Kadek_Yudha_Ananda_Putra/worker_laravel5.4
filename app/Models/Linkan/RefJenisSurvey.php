<?php

namespace App\Models\Linkan;

use Illuminate\Database\Eloquent\Model;

class RefJenisSurvey extends Model
{
    protected $table = SCHEMA_LINKAN . '.ref_jenis_survey';
}
