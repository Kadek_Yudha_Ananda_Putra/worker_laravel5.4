<?php

namespace App\Models\Linkan;

use Illuminate\Database\Eloquent\Model;

class SistemSetting extends Model
{
    protected $table = SCHEMA_LINKAN . '.sistem_setting';
}
