<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RefServices extends Model
{

    protected $table = 'ref_services';

    protected $guarded = [
        'id'
    ];
}
