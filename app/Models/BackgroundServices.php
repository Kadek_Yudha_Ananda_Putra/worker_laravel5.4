<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BackgroundServices extends Model
{

    protected $table = 'background_services';
    // protected $primaryKey = 'id';
    protected $guarded = ['id'];
    // protected $fillable = [
    //     'nama_service',
    //     'id_ref_service',
    //     'keterangan',
    //     'status',
    //     'id_timeline',
    //     'kode_tahun_ajaran',
    // ];
}
