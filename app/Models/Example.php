<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Example extends Model
{

    protected $table = 'public.example_jobs';
    protected $primaryKey = 'id';
}
