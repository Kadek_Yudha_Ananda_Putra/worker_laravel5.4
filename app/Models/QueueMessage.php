<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QueueMessage extends Model
{

    protected $table = 'queue_messages';

    protected $fillable = [
        'name',
        'message',
        'status',
        'id_background_service',
    ];
}
