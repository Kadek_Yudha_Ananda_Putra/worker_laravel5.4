<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageMemberModel extends Model
{
    protected $table = 'member_messages';
    protected $fillable = [
        'group_id',
        'member_id',
        'message',
        'is_sended',
        'created_at',
        'updated_at'
    ];

    public function peserta() {
        return $this->belongsTo(Peserta::class, 'peserta_id', 'id');
    }
}
