<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageGroupModel extends Model
{
    protected $table = 'group_messages';
    protected $fillable = [
        'group_id',
        'message',
        'is_finisheed',
        'start_sended_at',
        'finish_sended_at',
        'created_at',
        'updated_at',
    ];
}
