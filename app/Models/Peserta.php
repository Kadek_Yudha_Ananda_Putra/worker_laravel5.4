<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Peserta extends Model
{
    protected $table = 'peserta';
    protected $fillable = [
        'nama',
        'nomor_telepon',
        'is_sended',
        'created_at',
        'updated_at',
    ];

    public function groups(){
        return $this->belongsToMany(BroadcastGroupMember::class);
    }

    public function messageMember() {
        return $this->hasMany(MessageMemberModel::class, 'peserta_id', 'id');
    }
}
