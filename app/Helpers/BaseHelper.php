<?php

// namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use App\Helpers\Uuid;
use App\Models\BackgroundServices;
use App\Helpers\JWT;

function getSystemSetting($name, $default = null)
{
    $_settingTable = 'public.system_settings';
    $query = DB::table($_settingTable)->where(["name"  => $name])->first();
    if (!empty($query)) {
        return $query->value;
    }
    return $default;
}

function setSystemSetting($name, $value)
{
    $_settingTable = 'public.system_settings';

    $query = DB::table($_settingTable)->where(['name' => $name])->first();

    if (!empty($query)) {
        DB::table($_settingTable)->where(['name' => $name])->update(['value' => $value]);
    } else {
        DB::table($_settingTable)->insert([
            'name' => $name,
            'value' => $value
        ]);
    }
}
function getCountryById($countryId)
{
    $data = DB::table('public.ref_country')->where(['id' => $countryId])->first();
    return $data;
}

function getUUID($versi = 4)
{
    if ($versi == 4) {
        $return = Uuid::v4();
    } else {
        $return = Uuid::v4();
    }
    return $return;
}

function postRequest($url, $data)
{
    $jsonData = json_encode($data);
    $token = getSystemSetting('worker_api_key');
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => $jsonData,
        CURLOPT_SSL_VERIFYPEER => FALSE,
        CURLOPT_SSL_VERIFYHOST => FALSE,
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'Authorization:' . $token
        ),
    ));
    ob_start();
    $response = curl_exec($curl);
    ob_end_clean();
    $err = curl_error($curl);

    curl_close($curl);
    return $response;
}

function getRequest($url)
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
        ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    return $response;
}

function getSeries($column, $dataRaw)
{
    $return = null;
    foreach ($dataRaw as $key => $data) {
        $dataArray = (array)$data;
        $return[] = $dataArray[$column];
    }

    return $return;
}


function setDoneService($idBackService, $doneAt = null)
{
    if (empty($doneAt)) {
        $doneAt = date('Y-m-d H:i:s');
    }
    return BackgroundServices::where(['id' => $idBackService])->update([
        'status'    => 'DONE',
        'done_at'   => $doneAt,
    ]);
}

function setDefaultValue($value, $alias = '-')
{
    if (empty($value)) {
        return $alias;
    }
    return $value;
}

function castingNoHP($noHP)
{
    $noHP = trim($noHP);
    $noHP = str_replace(' ', '', $noHP);
    $noHP = str_replace('+62', '0', $noHP);
    $noHP = str_replace('(', '', $noHP);
    $noHP = str_replace(')', '', $noHP);
    $noHP = str_replace('-', '', $noHP);
    $result = $noHP;
    if (substr($noHP, 0, 1) == '0') {
        $result = str_replace('0', '', substr($noHP, 0, 1)) . substr($noHP, 1);
    }
    $noHP = $result;
    return $noHP;
}

function getEncrypt($string, $jwtKey = null)
{
    $salt = DEFAULT_SALT;
    $val = $salt . $string;
    if (empty($jwtKey)) {
        $jwtKey = config('jwt.jwt_key');
    }
    $hash = JWT::encode($val, $jwtKey);
    return $hash;
}

function getDecrypt($string, $verify = ['HS256'])
{
    $salt = DEFAULT_SALT;
    $jwtKey = config('jwt.jwt_key');
    $decode = JWT::decode($string, $jwtKey, $verify);
    $hasil = str_replace($salt, '', $decode);
    return $hasil;
}

function getZonaWaktuIndonesia()
{
    return getSystemSetting('timezone_indonesia');
}


function getRequestForMigrasi($url)
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_SSL_VERIFYPEER => FALSE,
        CURLOPT_SSL_VERIFYHOST => FALSE,
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
        ),
    ));
    ob_start();
    $response = curl_exec($curl);
    ob_end_clean();
    $err = curl_error($curl);

    curl_close($curl);
    return $response;
}
