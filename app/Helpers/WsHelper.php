<?php

function sendWa($phone, $text, $urlfile = null)
{
    $phone = $phone;
    $send = dswa_send($phone, $text, $urlfile);
    return ['status' => $send, 'message' => $text];
}

function sendWaMultiple($datas)
{
    $config = dswa_Configuration('send');
    for ($i = 0; $i < count($datas); $i++) {
        $jsonData = null;
        $jsonData = [
            'api_key' => $config['api_key'],
            'phone' => $datas[$i]['phone'],
            'text' => $datas[$i]['text']
        ];
        if (!empty($datas[$i]['media_url'])) {
            $jsonData['media_url'] = $datas[$i]['media_url'];
        }
        dswa_request($config['url'], $jsonData);
    }
}

function dswa_GetToken()
{
    return getSystemSetting('ws_api_key');
}

/**
 * dswa_Configuration
 *
 * @param  $type [send, connect, getqr]
 * @return void
 */
function dswa_Configuration($type = 'send')
{
    $data['api_key'] = dswa_GetToken();
    if ($type = 'send') {
        $data['url'] = getSystemSetting('ws_url_send');
    } elseif ($type == 'connect') {
        $data['url'] = getSystemSetting('ws_url_connect');
    } elseif ($type == 'getqr') {
        $data['url'] = getSystemSetting('ws_url_getqr');
    }
    return $data;
}

function dswa_send($phone, $text, $urlfile = null)
{
    $first = substr($phone, 0, 1);
    if ($first == '0') {
        $panjang = strlen($phone);
        $phone = '62' . substr($phone, - ($panjang - 1));
    }

    $config = dswa_Configuration('send');

    $jsonData = [
        'api_key' => $config['api_key'],
        'phone' => $phone,
        'text' => $text
    ];

    if (!empty($urlfile)) {
        $jsonData['media_url'] = $urlfile;
    }
    ob_start();
    //untuk menghilang print atau echo
    $return = dswa_request($config['url'], $jsonData);
    ob_end_clean();
    return $return;
}

function footerWa()
{
    $zonaWaktuIndonesia = getZonaWaktuIndonesia();
    $message = "\n\n";
    $message .= "_Pesan ini dihasilkan oleh komputer dan tidak perlu dijawab._";
    $message .= "\n---------------------------------------\n";
    $message .= "Terkirim pada: " . date("Y-m-d H:i:s") . ' ' . $zonaWaktuIndonesia;

    return $message;
}

function messageWaBody($nama, $messageBody, $forClient = false, $isSiakad = true)
{
    if ($nama != null) {
        $message = "Halo *" . $nama . "*";
        $message .= "\n\n";
    }
    $message .= $messageBody;
    $apl = $isSiakad ? 'Siakad' : 'PMB';
    if ($forClient == true) {
        $message .= "\n\n";
        $message .= date('Y') . ' | ' . $apl . ' ' . getSystemSetting('instansi_name') . ' | All Rights Reserved';
    }

    $message .= footerWa();

    return $message;
}

function dswa_request($url, $jsonData)
{
    // PHP Code
    //Initiate cURL.
    $ch = curl_init($url);
    //Encode the array into JSON.
    $jsonDataEncoded = json_encode($jsonData);
    //Tell cURL that we want to send a POST request.
    curl_setopt($ch, CURLOPT_POST, 1);

    //Attach our encoded JSON string to the POST fields.
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

    //Set the content type to application/json
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

    //Execute the request
    ob_start();
    $result = curl_exec($ch);
    ob_end_clean();
    return $result;
}
