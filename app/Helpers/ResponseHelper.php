<?php


function responseJson($status, $response)
{
    $data = !empty($response['data']) ? $response['data'] : null;
    $error = !empty($response['error']) ? $response['error'] : null;
    $return = [
        'code'   => $status,
        'message'       => $response['message'],
        'data'          => $data,
        'error'         => $error,
    ];
    return response()->json($return, $status);
}

function createdResponseJson($message, $data = null)
{
    $response['message'] =  $message;

    if ($data != null) {
        $response['data'] = $data;
    }

    return responseJson(201, $response);
}

function successResponseJson($message, $data = null)
{
    $response['message'] = $message;

    if ($data != null) {
        $response['data'] = $data;
    }

    return responseJson(200, $response);
}

function forbiddenResponseJson($message = null, $data = null)
{
    if ($message == null) {
        $message = "Anda tidak memiliki hak akses.";
    }

    if ($data != null) {
        $response['data'] = $data;
    }
    $response['message'] = $message;
    $response['error'] = $message;

    return responseJson(403, $response);
}

function notFoundResponseJson($message = null, $data = null)
{
    if ($message == null) {
        $message = "Data tidak ditemukan";
    }

    if ($data != null) {
        $response['data'] = $data;
    }
    $response['message'] = $message;
    $response["error"]     = $message;

    return responseJson(404, $response);
}

function methodNotAllowedResponseJson($message = null, $data = null)
{
    if ($message == null) {
        $message = "Metode HTTP Request tidak diijinkan server.";
    }

    if ($data != null) {
        $response['data'] = $data;
    }
    $response['message'] = $message;
    $response["error"]   = $message;

    return responseJson(405, $response);
}

function badRequestsResponseJson($message = null, $data = null)
{
    if ($message == null) {
        $message = "Format HTTP Request salah.";
    }

    if ($data != null) {
        $response['data'] = $data;
    }
    $response["message"] = $message;
    $response["error"]   = $message;

    return responseJson(400, $response);
}

function internalServerErrorResponseJson($message = null, $error = null, $data = null)
{
    if ($message == null) {
        $message = "Terjadi kesalahan server. Silahkan menghubungi bagian IT.";
    }

    $response["message"] = $message;
    $response["error"]   = $message;

    if (!empty($error)) {
        $response['error'] = $error;
    }

    if ($data != null) {
        $response['data'] = $data;
    }

    return responseJson(500, $response);
}
