<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $needAuth = ['api'];
    // authorization without auth base laravel,
    // karena end point api ke siakad
    public function __construct(Request $request)
    {
        $current = $request->segment(1);
        if (in_array($current, $this->needAuth)) {
                $this->authentikasi($request);
        }
    }

    public function authentikasi($request)
    {
        $apiKey = getSystemSetting('worker_api_key');
        $headers = $request->header();
        if (array_key_exists('authorization', $headers) && !empty($headers['authorization'])) {
            if ($apiKey != $headers['authorization'][0]) {
                header("Content-Type: application/json");
                http_response_code(401);
                echo json_encode([
                    'code'      => 401,
                    'message'   => 'Unathorized/Invalid Token',
                    'error'     => 'Unathorized/Invalid Token',
                    'data'      => null,
                ]);
                die;
            }
        } else {
            header("Content-Type: application/json");
            http_response_code(401);
            echo json_encode([
                'code'      => 401,
                'message'   => 'Unathorized/Invalid Token',
                'error'     => 'Unathorized/Invalid Token',
                'data'      => null,
            ]);
            die;
        }
    }
}