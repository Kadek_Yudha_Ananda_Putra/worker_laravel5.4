<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\linkansurveilink;
use App\Models\Linkan\RefBroadcast;
use App\Models\Linkan\JadwalSurvei;
use App\Models\Linkan\RefJenisSurvey;
use App\Models\MessageGroupModel;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class LinkanSendSurvei extends Controller
{
    public function __construct()
    {
    }

    public function index()
    {
        $url = getSystemSetting('url_worker') . 'api/broadcastingditerima';
        echo $url;
    }

    public function broadcastSurvei(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $input = $request->all();

        $ref_broadcast = DB::table('ref_broadcast')
            ->select('pesan', 'kirim_permenit', 'delay_kirim')
            ->where('id_broadcast', $input['id_broadcast'])
            ->first();

        $delay_send_perminute = preg_replace("/[^0-9]/", "", $ref_broadcast->delay_kirim);
        linkansurveilink::dispatch($input['id_survey'], $input['id_broadcast'], $ref_broadcast->pesan, $ref_broadcast->kirim_permenit)->delay(Carbon::now()->addMinutes($delay_send_perminute));
        return successResponseJson('Kirim pesan secara massal berhasil diproses !');
    }
}
