<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Linkan\JadwalSurvei;
use App\Models\Linkan\SistemSetting;
use App\Models\Linkan\RefBroadcast;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Artisan;
use App\Models\MessageGroupModel;
use App\Models\MessageMemberModel;

class linkansurveilink implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $id_survey;
    protected $id_broadcast;
    protected $ref_pesan;
    protected $kirim_permenit;
    public function __construct($id_survey, $id_broadcast, $ref_pesan, $kirim_permenit)
    {
        date_default_timezone_set('Asia/Jakarta');
        $this->id_survey = $id_survey;
        $this->id_broadcast = $id_broadcast;
        $this->ref_pesan = $ref_pesan; 
        $this->kirim_permenit = $kirim_permenit;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        date_default_timezone_set('Asia/Jakarta');

        $get_send_perminute = preg_replace("/[^0-9]/", "", $this->kirim_permenit);

        if(empty($get_send_perminute) || $get_send_perminute == 0){
            // default sent is 5
            $get_send_perminute = 5;
        }
        $limit = $get_send_perminute;
        $alumni = DB::table('alumni')
        ->select(['no_hp', 'is_send', 'id_alumni'])
            ->where('judul_send', $this->id_survey)
            ->where('is_send', '0')
            ->orderBy('id_alumni', 'asc')
            ->limit($limit)
            ->get();


        if (count($alumni) > 0) {
            foreach ($alumni as $key => $value) {
                $text = str_replace(['{id_alumni}', '{id_survey}'], [$value->id_alumni, $this->id_survey], $this->ref_pesan);
                $this->sendlink($value->no_hp, $text);
                DB::table('alumni')
                    ->where('id_alumni', $value->id_alumni)
                    ->update([
                        'is_send' => '1',
                    ]);
                $random_sleep = mt_rand(1, 10);
                sleep($random_sleep);
                if ($key == count($alumni) - 1) {
                    $alumni = DB::table('alumni')
                    ->select(['no_hp', 'is_send'])
                        ->where('judul_send', $this->id_survey)
                        ->where('is_send', '0')
                        ->limit(1)
                        ->get();
                    if (!empty($alumni)) {
                        $endpoint = $this->getSystemSetting('url_worker');
                        $url =  $endpoint.'api/linkan/linksurvei';
                        $where = [
                            'id_survey' => $this->id_survey,
                            'id_broadcast' =>  $this->id_broadcast
                        ];
                        $this->dswa_request($url, $where);
                    }
                }
            }
        }
    }

    public function getSystemSetting($name, $default = null){
        $query = SistemSetting::where(["name"  => $name])->first();
        if (!empty($query)) {
            return $query->value;
        }
        return $default;
    }

    function sendlink($phone, $text, $urlfile = null){
        $send = $this->dswa_send($phone, $text, $urlfile);
        return ['status' => $send, 'message' => $text];
    }

    function dswa_GetToken()
        {
            return $this->getSystemSetting('ws_api_key');
        }

    function dswa_Configuration($type = 'send')
    {
        $data['api_key'] = $this->dswa_GetToken();
        if ($type = 'send') {
            $data['url'] = $this->getSystemSetting('ws_url_send');
        }
        return $data;
    }

    function dswa_send($phone, $text, $urlfile = null){
        $first = substr($phone, 0, 1);
        if ($first == '0') {
            $panjang = strlen($phone);
            $phone = '62' . substr($phone, - ($panjang - 1));
        }

        $config = $this->dswa_Configuration('send');

        $jsonData = [
            'api_key' => $config['api_key'],
            'phone' => $phone,
            'text' => $text
        ];

        if (!empty($urlfile)) {
            $jsonData['media_url'] = $urlfile;
        }
        ob_start();
        //untuk menghilang print atau echo
        $return = $this->dswa_request($config['url'], $jsonData);
        ob_end_clean();
        return $return;
    }

    function dswa_request($url, $jsonData)
    {
    // PHP Code
    //Initiate cURL.
    $ch = curl_init($url);
    //Encode the array into JSON.
    $jsonDataEncoded = json_encode($jsonData);
    //Tell cURL that we want to send a POST request.
    curl_setopt($ch, CURLOPT_POST, 1);

    //Attach our encoded JSON string to the POST fields.
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

    //Set the content type to application/json
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

    //Execute the request
    ob_start();
    $result = curl_exec($ch);
    ob_end_clean();
    return $result;
    }
}
